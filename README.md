# comb

![alt text](https://ya-znau.ru/information/userfiles/01/comb.jpg "Formulae")

## C.pl
Usage: `./C.pl n m`
Example: 
```
bash-5.1$ ./C.pl 36 3
number of different occasions is "7140"
probability is "3.71993326789901e+41/5.20999057128713e+37" or 0.000140056022408964
```

## A.pl
Usage: `./A.pl n m`
Example:
```
bash-5.1$ ./A.pl 36 3
number of different occasions is "42840"
probability is "3.71993326789901e+41/8.68331761881189e+36" or 2.33426704014939e-05
```
